/**
 * binary sorting algorithm O(log n)
 * @param  {Integer} x    item we are searching
 * @param  {Array}   list ordered list
 * @return {Integer}      found item or -1 if not found
 */
const binarySearch = (x, list) => {
  let [i, j] = [0, list.length];
  
  while (i < j) {
    const mid = Math.floor((i + j) / 2)
    console.log(`mid is ${list[mid]} in possition ${mid}`)
    if (list[mid] == x) return x

    if (x < list[mid]) {
      j = mid
      console.log(`item may be in the left, (i:${i}, j:${j})`)
    } else {
      i = mid
      console.log(`item may be in the right, (i:${i}, j:${j})`)
    }
  }

  return -1
}

const list = [1, 2, 9, 10, 23, 34, 50, 199]
const found = binarySearch(34, list)
console.log(`result: ${found}`)

module.exports = binarySearch