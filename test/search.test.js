const { expect } = require('chai')
const binarySearch = require('../src/binary-search')

describe('binary-search', () => {
  it('should find the element', () => {
    const list = [3, 6, 9, 20, 33, 37, 41, 55, 77, 100]
    expect(binarySearch(3, list)).to.be.eq(3)
    expect(binarySearch(6, list)).to.be.eq(6)
    expect(binarySearch(9, list)).to.be.eq(9)
    expect(binarySearch(20, list)).to.be.eq(20)
    expect(binarySearch(33, list)).to.be.eq(33)
    expect(binarySearch(37, list)).to.be.eq(37)
    expect(binarySearch(41, list)).to.be.eq(41)
    expect(binarySearch(55, list)).to.be.eq(55)
    expect(binarySearch(77, list)).to.be.eq(77)
    expect(binarySearch(100, list)).to.be.eq(100)
  })

  it('should return -1 if element not in the list', () => {
    const list = [3, 6, 9, 20, 33, 37, 41, 55, 77, 100]
    const actual = binarySearch(-1, list)
    expect(actual).to.be.eq(-1)
  })
})

